
terraform {
  backend "s3" {
    region      = "us-east-1"
    bucket      = "terraform-condenesr"
    key         = "frontend/terraform.tfstate"   

  }
}

provider "aws" {
  region = "us-east-1"
}


## Creates EC2 
## Assuming this AMI Credentials are known

resource "aws_instance" "instance" {
  ami                             = "ami-059e6ca64746299f0"   //This AMI no longer exists
  instance_type                   = "t3.micro"
  vpc_security_group_ids          = [aws_security_group.allow_ssh.id]
  subnet_id                       = data.terraform_remote_state.vpc.outputs.PUBLIC_SUBNETS[0]

  tags = {
    Name = "frontend"
  }
}

#Creates SG
resource "aws_security_group" "allow_ssh" {
  name                        = "allow_ssh_frontend_ami"
  description                 = "Allow SSH inbound traffic"

  ingress {
    description               = "SSH"
    from_port                 = 22
    to_port                   = 22
    protocol                  = "tcp"
    cidr_blocks               = ["0.0.0.0/0"]
  }

  egress {
    from_port                 = 0
    to_port                   = 0
    protocol                  = "-1"
    cidr_blocks               = ["0.0.0.0/0"]
  }
  ingress {
    description               = "SSH"
    from_port                 = 80
    to_port                   = 80
    protocol                  = "tcp"
    cidr_blocks               = ["0.0.0.0/0"]
  }

  tags                        = {
    Name                      = "allow_ssh_frontend_ami"
  }
}

###keeping the installation outsite just to make it loosely coupled
resource "null_resource" "dockerbuildandrun" {
    provisioner "remote-exec" {
    connection {

       // user     = jsondecode(data.aws_secretsmanager_secret_version.robo.secret_string)["SSH_USER"]  ( If you've AWS Serret Manager,use this)
           user    = centos  //Hardcoding as I don't have value at the moment
      //  password = jsondecode(data.aws_secretsmanager_secret_version.robo.secret_string)["SSH_PASS"]  ( If you've AWS Serret Manager,use this)
           password = DevOps321  //Hardcoding as I don't have value at the moment
           host     = aws_instance.instance.private_ip  
    }

    inline = [
      "docker build -t sanraman/frontend:v1",
      "docker run  -p 80:80 -d sanraman/frontend:v1",
      "echo Use this IP to access the Hello Wold APP $(curl ifconfig.co)"                        
    ]
  }
}



